import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm'; 
import { ProductEntity } from 'src/products/product.entity/product.entity';
import { ProductsService } from 'src/products/products.service';
import { Repository } from 'typeorm';
import { CategoryEntity } from './category.entity/category.entity';

@Injectable()
export class CategoriesService {

    constructor(@InjectRepository(CategoryEntity) private categoriesRepository: Repository<CategoryEntity>, 
                @Inject(ProductsService) private productsService: ProductsService) { }

    async getCategories(): Promise<CategoryEntity[]> {
        return await this.categoriesRepository.find();
    }

    async getCategory(_id: number): Promise<CategoryEntity> {
        return await this.categoriesRepository.findOneBy({ "id": _id });
        
    }

    async getCategoryProducts(_id: number): Promise<ProductEntity[]>{
        return await this.productsService.getProductByCategory(_id);
    }

    async createCategory(product: CategoryEntity){
        return await this.categoriesRepository.save(product);
    }

    async updateCategory(product: CategoryEntity) {
        this.categoriesRepository.save(product)
    }

    async deleteCategory(product: CategoryEntity) {
        this.categoriesRepository.delete(product);
    }

}

