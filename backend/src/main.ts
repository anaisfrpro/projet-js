import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();// pour autoriser les URL, sécurité web , configurer le cors pour ne pas se faire pirater
  await app.listen(3000);
}
bootstrap();