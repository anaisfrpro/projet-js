import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductsService } from './products.service';
import { ProductsController } from './products.controller';
import { ProductEntity } from './product.entity/product.entity';

@Module({
  imports: [TypeOrmModule.forFeature([ProductEntity])], //Pour faire connexion BDD et entité
  providers: [ProductsService], // quel service appeler ?
  controllers: [ProductsController], // quel controller appeler ?
  exports: [ProductsService] //importer dans l'autre module
})

export class ProductsModule {}